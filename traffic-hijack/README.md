# 流量劫持 

## 1. 默认情况下的规则 

1.启动测试pod

```
cat <<EOF|kubectl apply -f -
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
 name: sleep
spec:
 replicas: 1
 template:
   metadata:
     labels:
       app: sleep
   spec:
     containers:
     - name: sleep
       image: pstauffer/curl
       command: ["/bin/sleep", "3650d"]
       imagePullPolicy: IfNotPresent
       ports:
       - containerPort: 9080
EOF
```

2.进入pod的network namespace

```
$ minikube ssh
$ docker ps|grep sleep_sleep
bcb55f0292c4        a95052191e0d                 "/bin/sleep 3650d"       2 hours ago         Up 2 hours                                                                               k8s_sleep_sleep-c59b5c9ff-gv462_default_55388e81-f12c-11e8-b56f-08002736cf28_0

$ docker top  bcb55f0292c4
UID                 PID                 PPID                C                   STIME               TTY                 TIME                CMD
root                16511               29889               0                   04:53               pts/0               00:00:00            sh
root                29908               29889               0                   03:35               ?                   00:00:00            /bin/sleep 3650d
$ sudo nsenter -t 29908  -n
```
  
3.查看iptables的规则

```
# iptables -L -v -t nat
Chain PREROUTING (policy ACCEPT 1 packets, 60 bytes)
pkts bytes target     prot opt in     out     source               destination
   1    60 ISTIO_INBOUND  tcp  --  any    any     anywhere             anywhere
# 所有进入tcp流量全部转到ISTIO_INBOUND

Chain INPUT (policy ACCEPT 1 packets, 60 bytes)
pkts bytes target     prot opt in     out     source               destination

Chain OUTPUT (policy ACCEPT 2420 packets, 226K bytes)
pkts bytes target     prot opt in     out     source               destination
   9   540 ISTIO_OUTPUT  tcp  --  any    any     anywhere             anywhere
# 所有出去的tcp流量全部转到ISTIO_OUTPUT

Chain POSTROUTING (policy ACCEPT 2424 packets, 226K bytes)
pkts bytes target     prot opt in     out     source               destination

Chain ISTIO_INBOUND (1 references)
pkts bytes target     prot opt in     out     source               destination
   0     0 ISTIO_IN_REDIRECT  tcp  --  any    any     anywhere             anywhere             tcp dpt:9080
# 目的地址为9080端口的tcp流量转到ISTIO_IN_REDIRECT

Chain ISTIO_IN_REDIRECT (1 references)
pkts bytes target     prot opt in     out     source               destination
   0     0 REDIRECT   tcp  --  any    any     anywhere             anywhere             redir ports 15001
# 所有的tcp流量redirect到port:15001

Chain ISTIO_OUTPUT (1 references)
pkts bytes target     prot opt in     out     source               destination
   0     0 ISTIO_REDIRECT  all  --  any    lo      anywhere            !localhost
   5   300 RETURN     all  --  any    any     anywhere             anywhere             owner UID match 1337
   0     0 RETURN     all  --  any    any     anywhere             anywhere             owner GID match 1337
   0     0 RETURN     all  --  any    any     anywhere             localhost
   4   240 ISTIO_REDIRECT  all  --  any    any     anywhere             anywhere
# 所有目的地址非localhost的流量全部跳转到ISTIO_REDIRECT
# 来自onwer 1337(istio-proxy）的流量走POSTROUTING 
# 目标地址是localhost的走POSTROUTING
# 其余的流量全部转到ISTIO_REDIRECT

Chain ISTIO_REDIRECT (2 references)
pkts bytes target     prot opt in     out     source               destination
   4   240 REDIRECT   tcp  --  any    any     anywhere             anywhere             redir ports 15001
# 所有的tcp流量redirect到port:15001
```
  
  入站流量
  ![](1.png)

  出站流量
  ![](2.png)

4.总结

  - istio只劫持了tcp流量，对于其他比如udp，icmp流量没有做劫持
  
```
kubectl exec -it sleep-c59b5c9ff-gv462 -c sleep ping www.baidu.com
PING www.baidu.com (180.97.33.108): 56 data bytes
64 bytes from 180.97.33.108: seq=0 ttl=61 time=7.331 ms
64 bytes from 180.97.33.108: seq=1 ttl=61 time=7.240 ms
```

  - dst==localhost的流量并不走istio-proxy(可以理解为性能更好)
  - 为了不形成死循环，istio-proxy自己的流量会直接访问目的地址
  - dst-port为指定port的入站tcp流量和其他container发出的出站tcp流量, 会全部交给istio-proxy处理, 也就是如果不做任何配置, 是无法访问外网的tcp服务 
    
```
$ kubectl exec -it sleep-c59b5c9ff-gv462 -c sleep -- sh -c "curl -I http://www.baidu.com"
HTTP/1.1 404 Not Found
date: Mon, 26 Nov 2018 07:04:58 GMT
server: envoy
transfer-encoding: chunked

$ kubectl exec -it sleep-c59b5c9ff-gv462 -c sleep -- sh -c "curl -I https://www.baidu.com"
curl: (35) LibreSSL SSL_connect: SSL_ERROR_SYSCALL in connection to www.baidu.com:443
command terminated with exit code 3
```

## 2. 配置ServiceEntry对外访问


### 1.访问外部http服务

1.配置ServiceEntry

```
cat <<EOF | kubectl apply -f -
apiVersion: networking.istio.io/v1alpha3
kind: ServiceEntry
metadata:
  name: baidu-http
spec:
  hosts:
  - www.baidu.com
  ports:
  - number: 80
    name: http
    protocol: HTTP
  resolution: DNS
EOF


$ kubectl exec -it sleep-c59b5c9ff-gv462 -c sleep -- curl -I http://www.baidu.com
HTTP/1.1 200 OK
accept-ranges: bytes
cache-control: private, no-cache, no-store, proxy-revalidate, no-transform
content-length: 277
content-type: text/html
date: Mon, 26 Nov 2018 07:30:50 GMT
etag: "575e1f7c-115"
last-modified: Mon, 13 Jun 2016 02:50:36 GMT
pragma: no-cache
server: envoy
x-envoy-upstream-service-time: 24
```

可以看到在配置了ServiceEntry后，立即可以访问baidu的http服务, 从response header可以看到，是istio-proxy帮忙做了转发

### 2.访问外部https服务

1.在上面配置了ServiceEntry的基础上，尝试访问https服务

```
$ kubectl exec -it sleep-c59b5c9ff-gv462 -c sleep -- curl -I https://www.baidu.com
curl: (35) LibreSSL SSL_connect: SSL_ERROR_SYSCALL in connection to www.baidu.com:443
command terminated with exit code 35
```
可以看到访问失败，因为istio-proxy需要解析http数据, 不支持直接使用https访问


2.使用新的规则

```
cat <<EOF | kubectl apply -f -
apiVersion: networking.istio.io/v1alpha3
kind: ServiceEntry
metadata:
  name: baidu-https 
spec:
  hosts:
  - www.baidu.com
  ports:
  - number: 443
    name: https
    protocol: HTTPS
  resolution: DNS
---
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: baidu 
spec:
  hosts:
  - www.baidu.com
  tls:
  - match:
    - port: 443
      sni_hosts:
      - www.baidu.com
    route:
    - destination:
        host: www.baidu.com
        port:
          number: 443
EOF

kubectl exec -it sleep-c59b5c9ff-gv462 -c sleep -- curl -I https://www.baidu.com
HTTP/1.1 200 OK
Accept-Ranges: bytes
Cache-Control: private, no-cache, no-store, proxy-revalidate, no-transform
Connection: Keep-Alive
Content-Length: 277
Content-Type: text/html
Date: Mon, 26 Nov 2018 07:38:04 GMT
Etag: "575e1f74-115"
Last-Modified: Mon, 13 Jun 2016 02:50:28 GMT
Pragma: no-cache
Server: bfe/1.0.8.18
```
可以看到https访问成功了, 由于https， istio-proxy无法解析，需要定义virtualService指示访问地址。 response header可以看到是直接和baidu的服务做了通讯


## 3. 配置global.proxy.includeIPRanges对外访问

上述ServiceEntry的配置在很多情况下配置起来非常麻烦，所以可以配置集群级别的配置，放行流量 

[https://istio.io/zh/docs/tasks/traffic-management/egress/#%E7%9B%B4%E6%8E%A5%E8%B0%83%E7%94%A8%E5%A4%96%E9%83%A8%E6%9C%8D%E5%8A%A1](https://istio.io/zh/docs/tasks/traffic-management/egress/#%E7%9B%B4%E6%8E%A5%E8%B0%83%E7%94%A8%E5%A4%96%E9%83%A8%E6%9C%8D%E5%8A%A1)

1.删除ServiceEntry

```
$ kubectl delete serviceEntry --all
serviceentry.networking.istio.io "baidu-http" deleted
serviceentry.networking.istio.io "baidu-https" deleted
```

2.更新istio的配置

```
$ helm upgrade istio install/kubernetes/helm/istio --set global.proxy.includeIPRanges="172.17.0.0/16\,10.96.0.0/12"
```

`172.17.0.0/16` 为minikube的pod-cidr, `10.96.0.0/12` 为service-cluster-ip-range


3.访问baidu

```
$ kubectl exec -it sleep-c59b5c9ff-gv462 -c sleep -- curl -I http://www.baidu.com
HTTP/1.1 404 Not Found
date: Mon, 26 Nov 2018 08:20:29 GMT
server: envoy
transfer-encoding: chunked
```
发现还是无法访问

4.重启pod后再次尝试

```
$ kubectl exec -it sleep-c59b5c9ff-prs76 -c sleep -- curl -I http://www.baidu.com                                                                                        
HTTP/1.1 200 OK
Accept-Ranges: bytes
Cache-Control: private, no-cache, no-store, proxy-revalidate, no-transform
Connection: Keep-Alive
Content-Length: 277
Content-Type: text/html
Date: Mon, 26 Nov 2018 08:23:12 GMT
Etag: "575e1f7c-115"
Last-Modified: Mon, 13 Jun 2016 02:50:36 GMT
Pragma: no-cache
Server: bfe/1.0.8.18

$ kubectl exec -it sleep-c59b5c9ff-prs76 -c sleep -- curl -I https://www.baidu.com
HTTP/1.1 200 OK
Accept-Ranges: bytes
Cache-Control: private, no-cache, no-store, proxy-revalidate, no-transform
Connection: Keep-Alive
Content-Length: 277
Content-Type: text/html
Date: Mon, 26 Nov 2018 08:23:16 GMT
Etag: "575e1f7b-115"
Last-Modified: Mon, 13 Jun 2016 02:50:35 GMT
Pragma: no-cache
Server: bfe/1.0.8.18
```

可以看到现在访问http和https都已经没有问题

5.查看iptables配置

```
...

Chain ISTIO_OUTPUT (1 references)
 pkts bytes target     prot opt in     out     source               destination
    0     0 ISTIO_REDIRECT  all  --  any    lo      anywhere            !localhost
    2   120 RETURN     all  --  any    any     anywhere             anywhere             owner UID match 1337
    0     0 RETURN     all  --  any    any     anywhere             anywhere             owner GID match 1337
    0     0 RETURN     all  --  any    any     anywhere             localhost
    0     0 ISTIO_REDIRECT  all  --  any    any     anywhere             172.17.0.0/16
    0     0 ISTIO_REDIRECT  all  --  any    any     anywhere             10.96.0.0/12
    2   120 RETURN     all  --  any    any     anywhere             anywhere
...
```


可以看到iptables的`ISTIO_OUTPUT`变了，只有`172.17.0.0/16`和`10.96.0.0/12`会跳转到`ISTIO_REDIRECT`(转发给istio-proxy), 其他的包都是走默认流程

不重启不生效的原因也是这里，这边是对iptables的修改来进行流量控制，而iptables规则是由pod的init-container(istio-init)写入，所以现有的pod是无法生效includeIPRanges的修改

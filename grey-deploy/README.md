# Service mesh灰度发布 

## 1. 安装bookinfo服务, 并模拟内网和外网请求

```
kubectl apply -f grey-deploy/bookinfo.yaml
kubectl apply -f grey-deploy/user.yaml

kubectl exec $(kubectl get pod -l app=user-external -o jsonpath="{.items[0].metadata.name}") -c sleep -- sh -c "while true;do curl http://productpage:9080/productpage &> /dev/null && sleep 0.1; done" &

kubectl exec $(kubectl get pod -l app=user-internal -o jsonpath="{.items[0].metadata.name}") -c sleep -- sh -c "while true;do curl -H "internal-user:1" http://productpage:9080/productpage &> /dev/null && sleep 0.5; done" &
```

可以看到此时的流量拓扑
![](1.png)



## 2. 创建流量规则，限制流量直接进入新版本

流量规则

```
cat <<EOF | kubectl apply -f -
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  name: productpage 
spec:
  host: productpage 
  subsets:
  - name: v1
    labels:
      version: v1
  - name: v2
    labels:
      version: v2
---
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: productpage 
spec:
  hosts:
  - productpage 
  http:
  - route:
    - destination:
        host: productpage
        subset: v1
EOF
```


## 3. 上线 productpage-v2
```
cat <<EOF | kubectl apply -f - 
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: productpage-v2
spec:
  replicas: 1
  template:
    metadata:
      labels:
        app: productpage
        version: v2
    spec:
      containers:
      - name: productpage
        image: istio/examples-bookinfo-productpage-v1:1.8.0
        imagePullPolicy: IfNotPresent
        ports:
        - containerPort: 9080
EOF
```
![](2.png)

此时productpage已经部署完成，但是没有流量进入

## 4. 指定特殊规则访问productpage-v2
http 可用过滤规则在:
[https://istio.io/docs/reference/config/istio.networking.v1alpha3/#HTTPMatchRequest](https://istio.io/docs/reference/config/istio.networking.v1alpha3/#HTTPMatchRequest)

这边用了http header，模拟内部人员测试 

```
cat <<EOF | kubectl apply -f -
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: productpage 
spec:
  hosts:
  - productpage 
  http:
  - match:
    - headers:
        internal-user:
          exact: "1"
    route:
    - destination:
        host: productpage 
        subset: v2
  - route:
    - destination:
        host: productpage
        subset: v1
EOF
```
![](3.png)

现在internal全部流量进入productpage-v2

## 5. 添加少量external流量进入productpage-v2

```
cat <<EOF | kubectl apply -f -
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: productpage 
spec:
  hosts:
  - productpage 
  http:
  - match:
    - headers:
        internal-user:
          exact: "1"
    route:
    - destination:
        host: productpage 
        subset: v2
  - route:
    - destination:
        host: productpage
        subset: v1
      weight: 90
    - destination:
        host: productpage
        subset: v2
      weight: 10
EOF
```

![](4.png)

可以看到10%的external流量已经进入productpage-v2


## 6. 全部流量进入productpage-v2

```
cat <<EOF | kubectl apply -f -
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: productpage 
spec:
  hosts:
  - productpage 
  http:
  - route:
    - destination:
        host: productpage
        subset: v2
EOF
```

![](5.png)

## 7. 下线productpage-v1
```
kubectl delete deploy productpage-v1

cat <<EOF | kubectl apply -f -
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  name: productpage 
spec:
  host: productpage 
  subsets:
  - name: v2
    labels:
      version: v2
EOF
```

![](6.png)

## 8. 多服务灰度发布
有一些情况，一个新功能依赖于很多的服务的共同更新, 对于灰度发布的服务依赖的服务，也需要是灰度版本

下面以上面10%流量进入灰度版本的情况举例，同时上线productpage-v3, reviews-v2, ratings-v2


### 1. 上线新的三个版本的服务, 都标记了deploy=grey, 并且通过流量控制限制流量进入

```
kubectl apply -f multi-service-destination-rule.yaml
```

### 2. 更新流量规则，reviews和ratings的灰度版本会接受前面服务的灰度版本的全部流量

```
cat <<EOF | kubectl apply -f -
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: reviews 
spec:
  hosts:
  - reviews 
  http:
  - match: 
    - sourceLabels:
        deploy: grey
    route:
    - destination:
        host: reviews
        subset: v2
  - route:
    - destination:
        host: reviews 
        subset: v1
---
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: ratings 
spec:
  hosts:
  - ratings 
  http:
  - match: 
    - sourceLabels:
        deploy: grey
    route:
    - destination:
        host: ratings 
        subset: v2
  - route:
    - destination:
        host: ratings 
        subset: v1
---
EOF
```

### 3. 开放内部流量和10%外部流量到productpage-v3

```
cat <<EOF | kubectl apply -f -
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: productpage 
spec:
  hosts:
  - productpage 
  http:
  - match:
    - headers:
        internal-user:
          exact: "1"
    route:
    - destination:
        host: productpage 
        subset: v3
  - route:
    - destination:
        host: productpage
        subset: v2
      weight: 90
    - destination:
        host: productpage
        subset: v3
      weight: 10
EOF
```

![](7.png)

对于访问到灰度服务的用户，整个服务链路中全部都是灰度服务